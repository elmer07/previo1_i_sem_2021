/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package previo1_ada;

/**
 *
 * @author coloque acá su nombres y código
 */
public class Previo1_ADA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int vector1[]={2,4,5,1,3,2};
        int vector2[]={2,4,5,1,3};
        System.out.println("Número prohibido:"+getNumeroProhibido(vector1));
        System.out.println("Número prohibido:"+getNumeroProhibido(vector2));
    }
    
    
    public static int getNumeroProhibido(int vector[]) // valor 1+1+for1+1 = 3+3+10n+6n^2 = 6+10n+6n^2 Tn= O(n^2)
    {
        int aux=0; // valor 1
        int cont;  // valor 1
        for(int i=0;i<vector.length;i++){ // valor (1+(3+2+for2+if2)*n+2) = 1+((5+3+6n+2)*n)+2 = 3+10n+6n^2 --> for1
            aux=vector[i]; // valor 1
            cont=0;        // valor 1
            for(int j=0;j<vector.length;j++){ // valor (1+((3+3)*n)+2)= 3+6n --> for2
                if(aux==vector[j]){ // valor 1
                    cont++;  //  valor 2
                }
            }
            if(cont>1) //  valor 1 --> if2 = 2
                return aux; // valor 1
        }
        return -1;  // valor 1
    }
}
